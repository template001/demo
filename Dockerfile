
FROM docker.io/gesiscss/binder-r2d-g5b5b759-ajibal-2dcoding-38529b:2657d5e087daee122e333a09554bb2beaabc134f
ENV DEBIAN_FRONTEND=noninteractive
USER root


RUN apt update && \
    apt install -y git podman qemu-system-x86 qemu-kvm wget libvirt-daemon-system libvirt-clients bridge-utils && \
    wget https://github.com/containers/gvisor-tap-vsock/releases/download/v0.7.3/gvproxy-linux-amd64 -O /usr/libexec/podman/gvproxy && \
    chmod +x /usr/libexec/podman/gvproxy
